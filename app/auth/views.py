from flask import Blueprint, redirect, url_for, session
from flask_login import current_user, logout_user, login_required, login_user
from app.extensions import login_manager, db
from app.mod.models import User
from utils import *
from requests_oauthlib import OAuth2Session
from sqlalchemy.orm import sessionmaker
import requests

auth = Blueprint('auth', __name__, template_folder='templates')

Session = sessionmaker()


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@auth.route('/login')
def login():
    if current_user.is_authenticated:
        return redirect(url_for('mod.index'))
    callback = url_for('auth.authorized', _external=True)
    return google.authorize(callback=callback)


@auth.route(REDIRECT_URI)
@google.authorized_handler
def authorized(resp):
    if current_user.is_authenticated and current_user is not None:
        return redirect(url_for('mod.index'))
    access_token = resp['access_token']
    session['access_token'] = access_token, ''
    userinfo = requests.get(GOOGLE_OAUTH2_USERINFO_URL, params=dict(
        access_token=access_token,
    )).json()
    user = User.query.filter_by(google_id=userinfo['id']).first()
    if user is None:
        user = User(str(userinfo['id']), userinfo['email'], userinfo['name'], userinfo['picture'])
        db.session.add(user)
        db.session.commit()
    login_user(user, remember=True)
    return redirect(url_for('mod.index'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('mod.index'))


@google.tokengetter
def get_access_token():
    return session.get('access_token')
