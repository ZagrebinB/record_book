from app.extensions import oauth


class AuthConfig:
    CLIENT_ID = ('383145522226-dkhiim2qfa5rmn0d53e9qv82m1119ra2'
                 '.apps.googleusercontent.com')
    CLIENT_SECRET = 'oNa_PeSzPhp6SvuhcLvlTwC6'
    REDIRECT_URI = '/reddit_callback'
    AUTH_URI = 'https://accounts.google.com/o/oauth2/auth'
    TOKEN_URI = 'https://accounts.google.com/o/oauth2/token'
    USER_INFO = 'https://www.googleapis.com/userinfo/v2/me'
    SCOPE = ['https://www.googleapis.com/auth/userinfo.email',
             'https://www.googleapis.com/auth/userinfo.profile']


GOOGLE_OAUTH2_USERINFO_URL = 'https://www.googleapis.com/oauth2/v2/userinfo'
GOOGLE_CLIENT_ID = '383145522226-dkhiim2qfa5rmn0d53e9qv82m1119ra2.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = 'oNa_PeSzPhp6SvuhcLvlTwC6'
REDIRECT_URI = "/reddit_callback"  # one of the Redirect URIs from Google APIs console

# oauth object in extension file
google = oauth.remote_app('google',
                          base_url='https://www.google.com/accounts/',
                          authorize_url='https://accounts.google.com/AccountChooser?continue=https://accounts.google.com/o/oauth2/auth?scope%3Dhttps://www.googleapis.com/auth/userinfo.email%26response_type%3Dcode%26redirect_uri%3Dhttp://127.0.0.1:5000/reddit_callback%26client_id%3D383145522226-dkhiim2qfa5rmn0d53e9qv82m1119ra2.apps.googleusercontent.com%26from_login%3D1%26as%3D17b8c008395145c0&btmpl=authsub&scc=1&oauth=1',
                          request_token_url=None,
                          request_token_params={'scope': 'https://www.googleapis.com/auth/userinfo.email',
                                                'response_type': 'code'},
                          access_token_url='https://accounts.google.com/o/oauth2/token',
                          access_token_method='POST',
                          access_token_params={'grant_type': 'authorization_code'},
                          consumer_key=GOOGLE_CLIENT_ID,
                          consumer_secret=GOOGLE_CLIENT_SECRET)
