from flask import Flask
from extensions import db, oid, login_manager
from auth.views import auth
from mod.views import mod
from api.views import api


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile('config.py')
    db.init_app(app)  # db object created in extension.py file

    login_manager.init_app(app)
    app.secret_key = app.config['SECRET_KEY']

    oid.init_app(app)  # initializing OpenID with app, fs_store_path in config file as OPENID_FS_STORE_PATH

    app.register_blueprint(auth)
    app.register_blueprint(mod)
    app.register_blueprint(api)

    return app
