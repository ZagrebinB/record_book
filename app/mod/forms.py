from flask_wtf import Form
from wtforms import StringField

WTF_CSRF_ENABLED = False


class StatementForm(Form):
    mark = StringField('mark')
    student_id = 0
