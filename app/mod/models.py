# coding=utf-8
from app.extensions import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    google_id = db.Column(db.String(30))
    email = db.Column(db.String(100))
    name = db.Column(db.String(100))
    avatar = db.Column(db.String(200))

    def __init__(self, google_id, email, name, avatar):
        self.google_id = google_id
        self.email = email
        self.name = name
        self.avatar = avatar

    def __repr__(self):
        return '<User %r, email %r>' % (self.name, self.email)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        try:
            return unicode(self.id)  # python 2
        except NameError:
            return str(self.id)  # python 3


class Faculties(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    faculty_name = db.Column(db.String(32))
    user_faculty = db.relationship('Specializations', backref='faculty', lazy='dynamic')
    teacher_faculty = db.relationship('Cafedras', backref='teacher_caf', lazy='dynamic')


class Specializations(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cafedra_name = db.Column(db.String(32))
    faculty_id = db.Column(db.Integer, db.ForeignKey('faculties.id'))
    code = db.Column(db.Integer)
    user_specialization = db.relationship('Students', backref='specialization', lazy='dynamic')
    subject_specialization = db.relationship('Subjects', backref='subject_spec')
    statement = db.relationship('Allstatements', backref='all_statement_spec')


class Studyforms(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    study_form = db.Column(db.String(32))
    studying_form = db.relationship('Students', backref='student_study_form')


class Cafedras(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cafedra_name = db.Column(db.String(256))
    faculty_id = db.Column(db.Integer, db.ForeignKey('faculties.id'))
    teacher_cafedra = db.relationship('Teachers', backref='teacher_cafedra')

# Ученная степень
class Degrees(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    degree_name = db.Column(db.String(128))
    teacher_degree = db.relationship('Teachers', backref='teacher_degree')

# экзамен и т д
class Controlforms(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    form = db.Column(db.String(12))
    control_form = db.relationship('Statement', backref='control_form')
    statement_form = db.relationship('Allstatements', backref='all_statement_form')


class Students(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256))
    email = db.Column(db.String(100))
    form = db.Column(db.Integer, db.ForeignKey('studyforms.id'))
    course = db.Column(db.Integer)
    specialization_id = db.Column(db.Integer, db.ForeignKey('specializations.id'))
    student_statement = db.relationship('Statement', backref='student_statement')


class Administrators(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    name = db.Column(db.String(100))


class Teachers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    email = db.Column(db.String(100))
    degree_id = db.Column(db.Integer, db.ForeignKey('degrees.id'))
    cafedra_id = db.Column(db.Integer, db.ForeignKey('cafedras.id'))
    teacher_statement = db.relationship('Statement', backref='teacher_statement')
    statement = db.relationship('Allstatements', backref='all_statement_teacher')
    teach_subject = db.relationship('Subjectteachers', backref='teacher_subject')


class Subjects(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subject_name = db.Column(db.String(64))
    specialization_id = db.Column(db.Integer, db.ForeignKey('specializations.id'))
    course = db.Column(db.Integer)
    subject_statement = db.relationship('Statement', backref='subject_statement')
    statement = db.relationship('Allstatements', backref='all_statement_subject')
    subject_teach = db.relationship('Subjectteachers', backref='subject_teacher')

# поле ведомости
class Statement(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    statement_id = db.Column(db.String(24))
    control_form_id = db.Column(db.Integer, db.ForeignKey('controlforms.id'))
    student_id = db.Column(db.Integer, db.ForeignKey('students.id'))
    subject_id = db.Column(db.Integer, db.ForeignKey('subjects.id'))
    teacher_id = db.Column(db.Integer, db.ForeignKey('teachers.id'))
    mark = db.Column(db.Integer, db.ForeignKey('marks.id'))
    date = db.Column(db.String(12))

    def __init__(self, number, control, student, subject, teacher, mark, date):
        self.statement_id = number
        self.control_form_id = control
        self.student_id = student
        self.subject_id = subject
        self.teacher_id = teacher
        self.mark = mark
        self.date = date

# все ведомости
class Allstatements(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    statement_number = db.Column(db.String(64))
    subject = db.Column(db.Integer, db.ForeignKey('subjects.id'))
    teacher = db.Column(db.Integer, db.ForeignKey('teachers.id'))
    control = db.Column(db.Integer, db.ForeignKey('controlforms.id'))
    specialization = db.Column(db.Integer, db.ForeignKey('specializations.id'))
    course = db.Column(db.Integer)
    date = db.Column(db.String(48))
    closed = db.Column(db.String(12))

    def __init__(self, number, subject, teacher, control, specialization, course):
        self.statement_number = number
        self.subject = subject
        self.teacher = teacher
        self.control = control
        self.specialization = specialization
        self.course = course

# оценки А, 100 и тд
class Marks(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    point = db.Column(db.Integer)
    letter = db.Column(db.String(4))
    mark = db.Column(db.Integer)
    examen_mark = db.Column(db.String(16))
    credit_mark = db.Column(db.String(16))
    statement_mark = db.relationship('Statement', backref='statement_mark')


class Subjectteachers(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    subjects_id = db.Column(db.Integer, db.ForeignKey('subjects.id'))
    teachers_id = db.Column(db.Integer, db.ForeignKey('teachers.id'))
