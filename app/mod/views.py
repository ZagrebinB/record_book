from urllib2 import Request, urlopen, URLError
from app.mod.models import *
from flask import Blueprint, g, redirect, url_for, session, request, flash, render_template, jsonify
from flask_login import login_required, current_user
import json

mod = Blueprint('mod', __name__, url_prefix='', template_folder='templates', static_folder='static')


@mod.before_request
def before_request():
    g.user = current_user


@mod.route('/')
def index():
    if current_user.is_authenticated:
        return render_template('index.html', user=g.user, title='Home',
                               Teachers=Teachers, Students=Students, Admins=Administrators)
    else:
        redirect(url_for('auth.login'))
    access_token = session.get('access_token')
    if access_token is None:
        return redirect(url_for('auth.login'))
    access_token = access_token[0]
    headers = {'Authorization': 'OAuth ' + access_token}
    req = Request('https://www.googleapis.com/oauth2/v1/userinfo',
                  None, headers)
    try:
        res = urlopen(req)
    except URLError, e:
        if e.code == 401:
            # Unauthorized - bad token
            session.pop('access_token', None)
            return redirect(url_for('auth.login'))
        return res.read()
    return redirect(url_for('auth.login'))


@mod.route('/my_record_book')
@login_required
def my_record_book():
    if not Students.query.filter_by(email=g.user.email).first():
        return render_template('Unauthorized.html', user=g.user,
                               title='Error')
    user = Students.query.filter_by(email=g.user.email).first().id
    stats = Statement.query.filter_by(student_id=user)
    return render_template('my_record_book.html',
                           user=g.user,
                           title='Statement',
                           stats=stats,
                           marks=Marks,
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/administration')
@login_required
def administration():
    if not Administrators.query.filter_by(email=g.user.email).first():
        return render_template('Unauthorized.html', user=g.user,
                               title='Error')
    return render_template('administration.html', user=g.user,
                           title='Administration', Teachers=Teachers, Students=Students, Admins=Administrators)


@mod.route('/teacher_panel')
@login_required
def teacher_panel():
    if not Teachers.query.filter_by(email=g.user.email).first():
        return render_template('Unauthorized.html', user=g.user,
                               title='Error')
    return render_template('teacher_panel.html',
                           user=g.user,
                           title='Teacher Panel',
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/create_statement', methods=['GET', 'POST'])
@login_required
def create_statement():
    if request.method == 'POST':
        specialization = request.form.get('statement_specialization')
        teacher = request.form.get('statement_teacher')
        subject = request.form.get('statement_subject')
        number = request.form.get('statement_number')
        course = request.form.get('statement_course')
        control = request.form.get('statement_control_form')
        if specialization and teacher and subject and number and course and control is not None:
            statement = Allstatements(number, subject, teacher, control, specialization, course)
            db.session.add(statement)
            db.session.commit()
        if statement is not None:
            students = Students.query.all()
            for s in students:
                if s.specialization_id == statement.specialization and s.course == statement.course:
                    student_statement = Statement(number, control, s.id, subject, teacher, '', '')
                    db.session.add(student_statement)
                    db.session.commit()
                else:
                    flash('Something is missing')
            flash('Statement created successfully')
            return redirect(url_for('mod.index'))
    teacher_list = Teachers.query.all()
    subjects = Subjects.query.all()
    controlforms = Controlforms.query.all()
    specializations = Specializations.query.all()
    return render_template('create_statement.html',
                           user=g.user,
                           title='Create Statement',
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators,
                           TeacherList=teacher_list,
                           SubjectList=subjects,
                           ControlFormList=controlforms,
                           SpecializationList=specializations)


@mod.route('/show_statement', methods=['GET', 'POST'])
@login_required
def show_statement():
    teacher = Teachers.query.filter_by(email=g.user.email).first().id
    statements = Allstatements.query.filter_by(teacher=teacher)
    return render_template('show_statement.html',
                           state=statements,
                           user=g.user,
                           title='Statements List',
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/show_statement/<statement_view>')
@login_required
def statements(statement_view):
    selected_statement = Allstatements.query.filter_by(statement_number=statement_view).first()
    statement = Statement.query.all()
    return render_template('view_statement.html',
                           user=g.user,
                           title='View Statement',
                           marks=Marks,
                           stats=statement,
                           state=selected_statement,
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/statements_list')
@login_required
def statements_list():
    teacher = Teachers.query.filter_by(email=g.user.email).first().id
    statements = Allstatements.query.filter_by(teacher=teacher)
    return render_template('statements_list.html',
                           state=statements,
                           user=g.user,
                           title='Statements List',
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/statements_list/<name>', methods=['GET', 'POST'])
@login_required
def statement(name):
    selected_statement = Allstatements.query.filter_by(statement_number=name).first()
    stats = Statement.query.all()
    if request.method == 'POST':
        date = request.form.get('date')
        for s in stats:
            if s.statement_id == selected_statement.statement_number and s.subject_id == selected_statement.subject:
                abd = s.student_id
                mark = str(abd)
                student_id = str(abd * 200)
                mark = request.form.get(mark)
                student = request.form.get(student_id)
                g.temp = Statement.query.filter_by(subject_id=s.subject_id).filter_by(student_id=student).first()
                g.temp.date = date
                if not g.temp.mark:
                    g.temp.mark = mark
                    db.session.add(g.temp)
                    db.session.commit()
        g.tmp = selected_statement
        g.tmp.closed = 'closed'
        g.tmp.date = date
        db.session.add(g.tmp)
        db.session.commit()
        return redirect(url_for('mod.statements', statement_view=selected_statement.statement_number))
    return render_template('statement.html',
                           user=g.user,
                           title='Statement',
                           state=selected_statement,
                           stats=stats,
                           Teachers=Teachers,
                           Students=Students,
                           Admins=Administrators)


@mod.route('/get_the_subject/<int:statement_specialization>/<int:statement_course>', methods=['GET'])
@login_required
def get_the_subject(statement_specialization, statement_course):
    subjects = Subjects.query.filter_by(specialization_id=statement_specialization).filter_by(course=statement_course)
    to_json = {}
    for subject in subjects:
        dict = {subject.id: subject.subject_name}
        to_json.update(dict)
    dumped_json = json.dumps(to_json)
    return dumped_json


@mod.route('/get_the_teachers/<int:subject>', methods=['GET'])
@login_required
def get_the_teachers(subject):
    teachers = Subjectteachers.query.filter_by(subjects_id=subject)
    to_json = {}
    for teacher in teachers:
        dict = {teacher.teachers_id: teacher.teacher_subject.name}
        to_json.update(dict)
    dumped_json = json.dumps(to_json)
    return dumped_json


@mod.route('/record_book_api/<string:email>', methods=['GET'])
def record_book_api(email):
    to_json = {}
    print email
    student = Students.query.filter_by(email=email).first()
    for S in Statement.query.filter_by(student_id=student.id):
        control_form = S.control_form.form
        if control_form == 'Exam':
            mark = S.statement_mark.examen_mark
        else:
            mark = S.statement_mark.credit_mark
        dict = {S.subject_id: {"subject": S.subject_statement.subject_name,
                               "teacher": S.teacher_statement.name,
                               "control_form": control_form,
                               "mark": {
                                   "points": S.mark,
                                   "letter": S.statement_mark.letter,
                                   "mark": mark
                               }}}
        to_json.update(dict)
    dumped_json = json.dumps(to_json, encoding='utf-8')
    return dumped_json
