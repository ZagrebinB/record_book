from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
allstatements = Table('allstatements', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('statement_number', String(length=64)),
    Column('subject', Integer),
    Column('teacher', Integer),
    Column('control', Integer),
    Column('specialization', Integer),
    Column('course', Integer),
    Column('closed', String(length=12)),
)

statement = Table('statement', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('statement_id', VARCHAR(length=24)),
    Column('control_form_id', INTEGER),
    Column('student_id', INTEGER),
    Column('subject_id', INTEGER),
    Column('teacher_id', INTEGER),
    Column('mark', INTEGER),
    Column('date', VARCHAR(length=48)),
    Column('closed', VARCHAR(length=12)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['allstatements'].columns['closed'].create()
    pre_meta.tables['statement'].columns['closed'].drop()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['allstatements'].columns['closed'].drop()
    pre_meta.tables['statement'].columns['closed'].create()
