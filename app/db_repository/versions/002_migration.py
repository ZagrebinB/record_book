from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
allstatements = Table('allstatements', pre_meta,
    Column('id', INTEGER, primary_key=True, nullable=False),
    Column('statement_number', VARCHAR(length=64)),
    Column('sudject', INTEGER),
    Column('teacher', INTEGER),
    Column('control', INTEGER),
    Column('specialization', INTEGER),
    Column('course', INTEGER),
)

allstatements = Table('allstatements', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('statement_number', String(length=64)),
    Column('subject', Integer),
    Column('teacher', Integer),
    Column('control', Integer),
    Column('specialization', Integer),
    Column('course', Integer),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['allstatements'].columns['sudject'].drop()
    post_meta.tables['allstatements'].columns['subject'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    pre_meta.tables['allstatements'].columns['sudject'].create()
    post_meta.tables['allstatements'].columns['subject'].drop()
