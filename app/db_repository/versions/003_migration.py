from sqlalchemy import *
from migrate import *


from migrate.changeset import schema
pre_meta = MetaData()
post_meta = MetaData()
statement = Table('statement', post_meta,
    Column('id', Integer, primary_key=True, nullable=False),
    Column('statement_id', String(length=24)),
    Column('control_form_id', Integer),
    Column('student_id', Integer),
    Column('subject_id', Integer),
    Column('teacher_id', Integer),
    Column('mark', Integer),
    Column('date', String(length=48)),
    Column('closed', String(length=12)),
)


def upgrade(migrate_engine):
    # Upgrade operations go here. Don't create your own engine; bind
    # migrate_engine to your metadata
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['statement'].columns['closed'].create()


def downgrade(migrate_engine):
    # Operations to reverse the above upgrade go here.
    pre_meta.bind = migrate_engine
    post_meta.bind = migrate_engine
    post_meta.tables['statement'].columns['closed'].drop()
