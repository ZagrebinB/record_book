from flask_sqlalchemy import SQLAlchemy
from flask_openid import OpenID
from flask_oauth import OAuth
from flask_login import LoginManager

db = SQLAlchemy()
oauth = OAuth()
oid = OpenID()
login_manager = LoginManager()

